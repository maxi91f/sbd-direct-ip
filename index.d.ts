export function createServer(options: Options): Promise<{
	/**
	 * Start the server listening
	 *
	 * @param {number} [port] Listen on this port
	 * @param {string} [host] Listen on this host address
	 */
	listen: (port?: number, host?: string) => void;
	/**
	 * Add an event handler
	 *
	 * @param {'message'} event Event type
	 * @param {(data) => void} handler Event handler
	 */
	on: (event: 'message', handler: (data: any) => void) => void;
	/**
	 * Remove an event handler
	 *
	 * @param {'message'} event Event type
	 * @param {(data) => void} handler Event handler
	 */
	off: (event: 'message', handler: (data: any) => void) => void;
}>;
export type LoggerFn = (...args: any[]) => void;
export type Logger = {
	/**
	 * Logger for log messages
	 */
	log?: LoggerFn;
	/**
	 * Logger for error messages
	 */
	error?: LoggerFn;
	/**
	 * Logger for warn messages
	 */
	warn?: LoggerFn;
	/**
	 * Logger for info messages
	 */
	info?: LoggerFn;
	/**
	 * Logger for debug messages
	 */
	debug?: LoggerFn;
};
/**
 * Server options
 */
export type Options = {
	/**
	 * Port to use for server
	 */
	port: number;
	/**
	 * Host to use for server
	 */
	host: string;
	/**
	 * Logger to use for logging
	 */
	logger: Logger;
};
