# sbd-direct-ip 0.1.1

Server to receive SDB DirectIP messages

[![pipeline status](https://gitlab.com/bytesnz/sbd-direct-ip/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/sbd-direct-ip/commits/main)
[![sbd-direct-ip on NPM](https://bytes.nz/b/sbd-direct-ip/npm)](https://npmjs.com/package/sbd-direct-ip)
[![license](https://bytes.nz/b/sbd-direct-ip/custom?color=yellow&name=license&value=AGPL-3.0)](https://gitlab.com/bytesnz/sbd-direct-ip/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/sbd-direct-ip/custom?color=yellowgreen&name=development+time&value=~3+hours)](https://gitlab.com/bytesnz/sbd-direct-ip/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/sbd-direct-ip/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/sbd-direct-ip/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/sbd-direct-ip/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

Can be used as a simple server to log incoming SBD DirectIP messages

```shell
npx sdbServer
```

or as a module to create the server and handle incoming messages

```js
import { createServer } 'sbd-direct-ip';

const server = createServer();

server.on('message', (data) => {
  console.log('Got a new SBD DirectIP message');
});

server.listen();
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](https://gitlab.com/bytesnz/sbd-direct-ip/issues) or
[email](mailto:contact-project+bytesnz-sbd-direct-ip-37034776-issue-@incoming.gitlab.com) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/sbd-direct-ip/issues?issue[confidential]=true)**

The source is hosted on [Gitlab](git+https://gitlab.com/bytesnz/sbd-direct-ip.git) and
uses [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.1] - 2022-06-16

### Changed

- Update binary-decoder to v0.2.0


## [0.1.0] - 2022-06-15

Initial version!

[0.1.1]: https://gitlab.com/bytesnz/sbd-direct-ip/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/sbd-direct-ip/tree/v0.1.0


[husky]: https://typicode.github.io/husky
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
