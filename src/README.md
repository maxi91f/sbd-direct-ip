# <!=package.json name> <!=package.json version>

<!=package.json description>

[![pipeline status](https://gitlab.com/bytesnz/<!=package.json name>/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/<!=package.json name>/commits/main)
[![<!=package.json name> on NPM](https://bytes.nz/b/<!=package.json name>/npm)](https://npmjs.com/package/<!=package.json name>)
[![license](https://bytes.nz/b/<!=package.json name>/custom?color=yellow&name=license&value=AGPL-3.0)](https://gitlab.com/bytesnz/<!=package.json name>/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/<!=package.json name>/custom?color=yellowgreen&name=development+time&value=~3+hours)](https://gitlab.com/bytesnz/<!=package.json name>/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/<!=package.json name>/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/<!=package.json name>/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/<!=package.json name>/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

Can be used as a simple server to log incoming SBD DirectIP messages

```shell
npx sdbServer
```

or as a module to create the server and handle incoming messages

```js
import { createServer } '<!=package.json name>';

const server = createServer();

server.on('message', (data) => {
  console.log('Got a new SBD DirectIP message');
});

server.listen();
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

The source is hosted on [Gitlab](<!=package.json repository.url>) and
uses [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

<!=CHANGELOG.md>

[husky]: https://typicode.github.io/husky
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
