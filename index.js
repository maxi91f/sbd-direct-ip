import { createDecoder } from 'binary-decoder';

import sdb from './messages/sbd.js';

import { createServer as createTcpServer } from 'node:net';
import { writeFile, stat, mkdir } from 'node:fs/promises';
import { join } from 'node:path';

/** @typedef {(...args) => void} LoggerFn */

/** @typedef {Object} Logger
 *
 * @property {LoggerFn} [log] Logger for log messages
 * @property {LoggerFn} [error] Logger for error messages
 * @property {LoggerFn} [warn] Logger for warn messages
 * @property {LoggerFn} [info] Logger for info messages
 * @property {LoggerFn} [debug] Logger for debug messages
 */

/** @typedef {Object} Options
 * Server options
 *
 * @property {number} port Port to use for server
 * @property {string} host Host to use for server
 * @property {Logger} logger Logger to use for logging
 */

/* eslint-disable no-console */
/** @type {Options} */
const defaultOptions = {
	port: process.env.PORT || 4567,
	host: process.env.HOST || 'localhost',
	logger: {
		error: (...data) => console.error(new Date().toISOString(), 'ERROR', ...data),
		info: (...data) => console.info(new Date().toISOString(), 'INFO', ...data),
		log: (...data) => console.log(new Date().toISOString(), 'LOG', ...data),
		warn: (...data) => console.warn(new Date().toISOString(), 'WARN', ...data),
		debug: process.env.DEBUG
			? (...data) => console.debug(new Date().toISOString(), 'DEBUG', ...data)
			: () => {}
	}
};
/* eslint-enable no-console */

/**
 * Create a Node Net server for receiving SBD DirectIP messages
 *
 * @param {Options} options Options for server
 */
export const createServer = async (options) => {
	// Populate options
	options = {
		...defaultOptions,
		...(options || {})
	};

	// Check raw data log folder exists
	if (options.logRawDataFolder) {
		try {
			const folderStat = await stat(options.logRawDataFolder);

			if (!folderStat.isDirectory()) {
				throw new Error('logRawDataFolder is not a folder');
			}
		} catch (error) {
			if (error.code === 'ENOENT') {
				await mkdir(options.logRawDataFolder);
			} else {
				throw error;
			}
		}
	}

	const decoder = createDecoder(sdb);

	const server = createTcpServer((connection) => {
		const parts = [];

		if (options.allowlist) {
			if (options.allowlist.indexOf(connection.remoteAddress) === -1) {
				options.logger?.info?.(
					'Received connection for client not in allowlist',
					connection.remoteAddress + ':' + connection.remotePort
				);
				connection.end();
				return;
			}
		}

		if (options.denylist) {
			if (options.denylist.indexOf(connection.remoteAddress) !== -1) {
				options.logger?.info?.(
					'Received connection for client in denylist',
					connection.remoteAddress + ':' + connection.remotePort
				);
				connection.end();
				return;
			}
		}

		options.logger?.info?.(
			'Client connected',
			connection.remoteAddress + ':' + connection.remotePort
		);

		connection.on('data', (data) => {
			options.logger?.debug('got data', data);
			parts.push(data);
		});

		connection.on('end', () => {
			const data = new Uint8Array(Buffer.concat(parts));

			let name = '';

			if (options.logRawDataFolder) {
				name = new Date().toISOString() + '.bin';
				writeFile(join(options.logRawDataFolder, name), data);
				name = ' in ' + name;
			}

      let decoded;
			try {
				decoded = decoder.decode(data);
			} catch (error) {
				options.logger?.error?.('error decoding message' + name, error.message);
        return;
			}

      options.logger?.info(`Received SBD DirectIP message`);
      options.logger?.debug?.('decoded message' + name, decoded.decodedMessage);

      if (handlers.message.length) {
        for (const handler of handlers.message) {
          handler(decoded.decodedMessage);
        }
      }
		});

		connection.on('close', () => {
			options.logger?.info?.(
				'Client disconnected',
				connection.remoteAddress + ':' + connection.remotePort
			);
		});
	});

	const handlers = {
		message: []
	};

	return {
		/**
		 * Start the server listening
		 *
		 * @param {number} [port] Listen on this port
		 * @param {string} [host] Listen on this host address
     * @param {() => void} callback Callback to run after the server has been
     *   started
		 */
		listen: (port, host, callback) => {
			server.listen(port || options.port, host || options.host, () => {
				options.logger?.log(`Listening on ${host || options.host}:${port || options.port}`);
        if (callback) {
          callback();
        }
			});
		},
    /**
     * Stop the server listening
     *
     * @param {(error: Error) => void} callback Callback to run after server
     *   has been stopped
     */
    close: (callback) => {
      server.close(callback);
    },
		/**
		 * Add an event handler
		 *
		 * @param {'message'} event Event type
		 * @param {(data) => void} handler Event handler
		 */
		on: (event, handler) => {
			if (!handlers[event]) {
				throw new Error(`Unknown event ${event}`);
			}

			if (handlers[event].indexOf(handler) === -1) {
				handlers[event].push(handler);
			}
		},
		/**
		 * Remove an event handler
		 *
		 * @param {'message'} event Event type
		 * @param {(data) => void} handler Event handler
		 */
		off: (event, handler) => {
			if (!handlers[event]) {
				throw new Error(`Unknown event ${event}`);
			}

			const index = handlers[event].indexOf(handler);

			if (index !== -1) {
				handlers[event].splice(index, 1);
			}
		}
	};
};

/**
 * Test if the SBD DirectIP message has an MO message
 */
export const hasMoMessage = (message) => message.data.moHeader && message.data.moPayload;
