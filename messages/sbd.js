/* @type {import('binary-decoder').MessageFormat} */
export default [
	{
		type: 'uint8',
		id: 'protocolVersion'
	},
	{
		type: 'uint16',
		id: 'messageLength'
	},
	{
		type: 'repeat',
		id: 'data',
		key: 'elementId',
		asObject: 'elementId',
		structure: [
			{
				type: 'uint8',
				id: 'elementId',
				enum: [
					{
						value: 1,
						label: 'MO Header IEI',
						key: 'moHeader'
					},
					{
						value: 2,
						label: 'MO Payload IEI',
						key: 'moPayload'
					},
					{
						value: 3,
						label: 'MO Location Information IEI',
						key: 'moLocation'
					},
					{
						value: 0x41,
						label: 'MT Header IEI',
						key: 'mtHeader'
					},
					{
						value: 0x42,
						label: 'MT Payload IEI',
						key: 'mtPayload'
					},
					{
						value: 0x44,
						label: 'MT Confirmation Message IEI',
						key: 'mtConfirmation'
					}
				]
			},
			{
				type: 'uint16',
				id: 'elementLength'
			},
			{
				type: 'map',
				id: 'elementContent',
				key: 'elementId',
				structures: {
					// MO Header IEI
					1: [
						{
							type: 'uint32',
							id: 'cdrReference'
						},
						{
							type: 'string',
							id: 'imei',
							length: 15
						},
						{
							type: 'uint8',
							id: 'sessionStatus',
							enum: [
								{
									value: 0,
									label: 'Session completed successfully'
								},
								{
									value: 1,
									label: 'MO completed successfully, MT too large'
								},
								{
									value: 2,
									label: 'MO completed successfully. Location unacceptable quality'
								},
								{
									value: 10,
									label: 'SBD session timed out before completion'
								},
								{
									value: 12,
									label: 'MO message being transferred too large'
								},
								{
									value: 13,
									label: 'RF link loss occured during session'
								},
								{
									value: 14,
									label: 'IMEI protocol anomaly occured during session'
								},
								{
									value: 15,
									label: 'IMEI prohibited from accessing the GSS'
								}
							]
						},
						{
							type: 'uint16',
							id: 'momsn'
						},
						{
							type: 'uint16',
							id: 'mtmsn'
						},
						{
							type: 'uint32',
							id: 'sessionTime'
						}
					],
					// MO Payload IEI
					2: [
						{
							type: 'variable',
							id: 'payload',
							format: 'raw',
							length: 'elementLength'
						}
					],
					// MO Location Information IEI
					3: [
						{
							type: 'bits',
							parts: [
								{
									bits: 4
								},
								{
									bits: 2,
									id: 'formatCode',
									enum: [
										{
											value: 0,
											label: 'Standard'
										},
										{
											value: 1,
											label: 'Reserved'
										},
										{
											value: 2,
											label: 'Reserved'
										},
										{
											value: 3,
											label: 'Reserved'
										}
									]
								},
								{
									bits: 1,
									id: 'northSouthIndicator'
								},
								{
									bits: 1,
									id: 'eastWestIndicator'
								}
							]
						},
						{
							type: 'uint8',
							id: 'latitudeDegrees'
						},
						{
							type: 'uint16',
							id: 'latitudeMinutes',
							factor: 1000
						},
						{
							type: 'uint8',
							id: 'longitudeDegrees'
						},
						{
							type: 'uint16',
							id: 'longitudeMinutes',
							factor: 1000
						},
						{
							type: 'uint32',
							id: 'cepRadius'
						}
					],
					// MT Header IEI
					0x41: [
						{
							type: 'string',
							id: 'messageId',
							length: 4
						},
						{
							type: 'string',
							id: 'imei',
							length: 15
						},
						{
							type: 'bitFlags',
							flags: [
								'Flush MT Queue',
								'Send Ring Alert',
								null,
								'Force Ring Alert',
								null,
								null,
								null,
								'Update SSD Location',
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								'High Priority Message'
							]
						}
					],
					// MO Payload IEI
					0x42: [
						{
							type: 'variable',
							id: 'payload',
							format: 'raw',
							length: 'elementLength'
						}
					],
					// MT Confirmation Message IEI
					0x44: [
						{
							type: 'string',
							id: 'messageId',
							length: 4
						},
						{
							type: 'string',
							id: 'imei',
							length: 15
						},
						{
							type: 'uint32',
							id: 'referenceId'
						},
						{
							type: 'int16',
							id: 'messageStatus',
							enum: [
								{
									value: 0,
									label: 'Sucessful. No payload'
								},
								{
									value: -1,
									label: 'Invalid IMEI'
								},
								{
									value: -2,
									label: 'Unknown IMEI'
								},
								{
									value: -3,
									label: 'Payload size exceeded'
								},
								{
									value: -4,
									label: 'Payload expected, but non received'
								},
								{
									value: -5,
									label: 'MT message queue full'
								},
								{
									value: -6,
									label: 'MT resources unavailable'
								},
								{
									value: -7,
									label: 'Violation of MT DirectIP protocol'
								},
								{
									value: -8,
									label: 'Ring alarts to the given IMEI are disabled'
								},
								{
									value: -9,
									label: 'The given IMEI is not attached'
								}
							]
						}
					]
				}
			}
		]
	}
];
