import { createDecoder } from 'binary-decoder';

import { readFile } from 'node:fs/promises';

import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

import { assert } from 'chai';

const __dirname = dirname(fileURLToPath(import.meta.url));

import typesStructure from '../messages/sbd.js';

let decoder = createDecoder(typesStructure);

assert.deepEqual(
	decoder.decode(new Uint8Array(await readFile(join(__dirname, 'mo.bin')))),
	{
		decodedMessage: {
			protocolVersion: 1,
			messageLength: 104,
			data: {
				moHeader: {
					elementId: { value: 1, label: 'MO Header IEI', key: 'moHeader' },
					elementLength: 28,
					cdrReference: 1234567,
					imei: '300034010123450',
					sessionStatus: { value: 0, label: 'Session completed successfully' },
					momsn: 54321,
					mtmsn: 12345,
					sessionTime: 1135950305
				},
				moPayload: {
					elementId: { value: 2, label: 'MO Payload IEI', key: 'moPayload' },
					elementLength: 5,
					payload: new Uint8Array([72, 101, 108, 108, 111])
				}
			}
		},
		nextByte: 42
	},
	'Decoder did not decode MO message correctly'
);
